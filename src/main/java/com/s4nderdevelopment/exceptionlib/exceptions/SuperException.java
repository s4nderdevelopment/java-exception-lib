package com.s4nderdevelopment.exceptionlib.exceptions;

public abstract class SuperException extends Exception {

    /**
     * Super class for all exceptions in this library
     */
    public SuperException(){
        
    }

    /**
     * Super class for all exceptions in this library
     * 
     * @param throwable which caused the exception.
     */
    public SuperException(Throwable throwable){
        super(throwable);
    }

    /**
     * @return a formatted message, provided by de subclass. This is the description of the error that occurred.
     */
    public abstract String getFormattedMessage();

    /**
     * @return a formatted message, provided by de subclass. This is the description of the error that occurred.
     */
    @Override
    public String getMessage(){
        return getFormattedMessage();
    }
}
