package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.UnauthorizedException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.INotLoggedInFormatter;

public class NotLoggedInException extends UnauthorizedException {

    private INotLoggedInFormatter messageFormatter;
    
    /**
     * The user is not logged in (401).
     * 
     * @param messageFormatter formatter for the status message
     */
    public NotLoggedInException(INotLoggedInFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format();
    }
    
}
