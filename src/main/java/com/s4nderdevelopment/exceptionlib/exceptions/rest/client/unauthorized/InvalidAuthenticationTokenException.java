package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.UnauthorizedException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidAuthenticationTokenFormatter;

public class InvalidAuthenticationTokenException extends UnauthorizedException {

    private IInvalidAuthenticationTokenFormatter messageFormatter;
    
    /**
     * The user made a request, but the authentication token was invalid (401).
     * 
     * @param messageFormatter formatter for the status message
     */
    public InvalidAuthenticationTokenException(IInvalidAuthenticationTokenFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format();
    }

}
