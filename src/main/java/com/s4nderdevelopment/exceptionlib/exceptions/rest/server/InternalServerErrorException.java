package com.s4nderdevelopment.exceptionlib.exceptions.rest.server;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class InternalServerErrorException extends RestException {

    private String message;

    /**
     * Internal Server Error (501) exception for REST API's.
     */
    public InternalServerErrorException(){
        this.message = "Internal server error";
    }

    /**
     * Internal Server Error (501) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public InternalServerErrorException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 500;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
