package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class UnauthorizedException extends RestException {

    private String message;

    /**
     * Unauthorized (401) exception for REST API's.
     */
    public UnauthorizedException(){
        this.message = "Unauthorized";
    }

    /**
     * Unauthorized (401) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public UnauthorizedException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 401;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
