package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.BadRequestException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IStringInvalidValueFormatter;

public class StringInvalidValueException extends BadRequestException {

    private IFieldNameFormatter fieldNameFormatter;
    private IStringInvalidValueFormatter messageFormatter;
    private String providedValue;

    /**
     * A provided string field contains an invalid value (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value, which was invalid
     */
    public StringInvalidValueException(IFieldNameFormatter fieldNameFormatter, IStringInvalidValueFormatter messageFormatter, String providedValue){
        this.fieldNameFormatter = fieldNameFormatter;
        this.messageFormatter = messageFormatter;
        this.providedValue = providedValue;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFieldName(){
        return fieldNameFormatter.format();
    }

    /**
     * Gets the provided value which was invalid.
     * 
     * @return the provided value which was invalid.
     */
    public String getProvidedValue(){
        return providedValue;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format(getFieldName(), providedValue);
    }

}
