package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.notfound;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.NotFoundException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.IEntityNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.notfound.IEntityNotFoundFormatter;

public class EntityNotFoundException extends NotFoundException {
    
    private IEntityNotFoundFormatter messageFormatter;
    private IEntityNameFormatter entityNameFormatter;

    /**
     * The requested entity was not found (404).
     * 
     * @param messageFormatter formatter for the status message
     * @param entityNameFormatter formatter for the entity name
     */
    public EntityNotFoundException(IEntityNotFoundFormatter messageFormatter, IEntityNameFormatter entityNameFormatter){
        this.messageFormatter = messageFormatter;
        this.entityNameFormatter = entityNameFormatter;
    }

    /**
     * Gets the requested entity name.
     * 
     * @return the requested entity name.
     */
    public String getEntityName(){
        return entityNameFormatter.format();
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format(getEntityName());
    }

}
