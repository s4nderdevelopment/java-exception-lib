package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class NotFoundException extends RestException {

    private String message;
    
    /**
     * Not Found (404) exception for REST API's.
     */
    public NotFoundException(){
        this.message = "Not found";
    }

    /**
     * Not Found (404) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public NotFoundException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 404;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
