package com.s4nderdevelopment.exceptionlib.exceptions.rest;

import com.s4nderdevelopment.exceptionlib.exceptions.SuperException;

public abstract class RestException extends SuperException {
    
    /**
     * Exception for REST API's.
     */
    public RestException(){
        
    }

    /**
     * Gets the HTTP status code for this exception.
     * @return the HTTP status code for the exception that occurred in the REST API request.
     */
    public abstract int getHttpStatusCode();

}
