package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class BadRequestException extends RestException {

    private String message;

    /**
     * Bad Request (400) exception for REST API's.
     */
    public BadRequestException(){
        this.message = "Bad request";
    }

    /**
     * Bad Request (400) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public BadRequestException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 400;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
