package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.BadRequestException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IPasswordTooWeakFormatter;

public class PasswordTooWeakException extends BadRequestException {
    
    private IFieldNameFormatter fieldFormatter;
    private IPasswordTooWeakFormatter messageFormatter;

    /**
     * The provided value of a password field was too weak (400).
     * 
     * @param fieldFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     */
    public PasswordTooWeakException(IFieldNameFormatter fieldFormatter, IPasswordTooWeakFormatter messageFormatter){
        this.fieldFormatter = fieldFormatter;
        this.messageFormatter = messageFormatter;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFormattedField(){
        return fieldFormatter.format();
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format(getFormattedField());
    }

}
