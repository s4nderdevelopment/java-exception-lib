package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class MethodNotAllowedException extends RestException {

    private String message;

    /**
     * Method Not Allowed (405) exception for REST API's.
     */
    public MethodNotAllowedException(){
        this.message = "Method not allowed";
    }

    /**
     * Method Not Allowed (405) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public MethodNotAllowedException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 405;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
