package com.s4nderdevelopment.exceptionlib.exceptions.rest.server.notimplemented;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.server.NotImplementedException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.server.notimplemented.IEndpointNotImplementedFormatter;

public class EndpointNotImplementedException extends NotImplementedException {

    private IEndpointNotImplementedFormatter messageFormatter;
    
    /**
     * Endpoint functionality has not been implemented yet (501).
     * 
     * @param messageFormatter formatter for the status message
     */
    public EndpointNotImplementedException(IEndpointNotImplementedFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format();
    }

}
