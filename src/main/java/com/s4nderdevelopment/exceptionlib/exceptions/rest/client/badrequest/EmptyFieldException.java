package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.BadRequestException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IEmptyFieldFormatter;

public class EmptyFieldException extends BadRequestException {

    private IFieldNameFormatter fieldNameFormatter;
    private IEmptyFieldFormatter messageFormatter;

    /**
     * There was no value provided for a field (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     */
    public EmptyFieldException(IFieldNameFormatter fieldNameFormatter, IEmptyFieldFormatter messageFormatter){
        this.fieldNameFormatter = fieldNameFormatter;
        this.messageFormatter = messageFormatter;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFieldName(){
        return fieldNameFormatter.format();
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format(getFieldName());
    }
}
