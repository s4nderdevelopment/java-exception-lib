package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class ForbiddenException extends RestException {

    private String message;

    /**
     * Forbidden (403) exception for REST API's.
     */
    public ForbiddenException(){
        this.message = "Forbidden";
    }

    /**
     * Forbidden (403) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public ForbiddenException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 403;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
