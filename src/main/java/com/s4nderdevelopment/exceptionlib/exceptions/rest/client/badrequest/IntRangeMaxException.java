package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.BadRequestException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMaxFormatter;

public class IntRangeMaxException extends BadRequestException {

    private IFieldNameFormatter fieldNameFormatter;
    private IIntRangeMaxFormatter messageFormatter;
    private int providedValue;
    private int maxValue;
    private Integer minValue;

    /**
     * The provided value of an integer field was too high (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value, which was too high
     * @param maxValue the maximum value
     */
    public IntRangeMaxException(IFieldNameFormatter fieldNameFormatter, IIntRangeMaxFormatter messageFormatter, int providedValue, int maxValue){
        this(fieldNameFormatter, messageFormatter, providedValue, maxValue, null);
    }

    /**
     * The provided value of an integer field was too high (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value, which was too high
     * @param maxValue the maximum value
     * @param minValue the minimum value, null if no minimum value
     */
    public IntRangeMaxException(IFieldNameFormatter fieldNameFormatter, IIntRangeMaxFormatter messageFormatter, int providedValue, int maxValue, Integer minValue){
        this.fieldNameFormatter = fieldNameFormatter;
        this.messageFormatter = messageFormatter;
        this.providedValue = providedValue;
        this.maxValue = maxValue;
        this.minValue = minValue;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFieldName(){
        return fieldNameFormatter.format();
    }

    /**
     * Gets the provided value which was too high.
     * 
     * @return the provided value.
     */
    public int getProvidedValue(){
        return providedValue;
    }

    /**
     * Gets the maximum value.
     * 
     * @return the maximum value.
     */
    public int getMaxValue(){
        return maxValue;
    }

    /**
     * Gets the minimum value, or null.
     * 
     * @return the minimum value, or null.
     */
    public Integer getMinValue(){
        return minValue;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format(getFieldName(), providedValue, maxValue, minValue);
    }
}
