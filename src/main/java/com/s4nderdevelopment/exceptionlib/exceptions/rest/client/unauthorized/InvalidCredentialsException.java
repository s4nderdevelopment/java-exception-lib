package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.UnauthorizedException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidCredentialsFormatter;

public class InvalidCredentialsException extends UnauthorizedException {

    private IInvalidCredentialsFormatter messageFormatter;
    
    /**
     * The user tried to log in, but provided invalid credentials (401).
     * 
     * @param messageFormatter formatter for the status message
     */
    public InvalidCredentialsException(IInvalidCredentialsFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format();
    }

}
