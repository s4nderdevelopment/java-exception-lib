package com.s4nderdevelopment.exceptionlib.exceptions.rest.server;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class NotImplementedException extends RestException {

    private String message;

    /**
     * Not Implemented (501) exception for REST API's.
     */
    public NotImplementedException(){
        this.message = "Not implemented";
    }

    /**
     * Not Implemented (501) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public NotImplementedException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 501;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
