package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.conflict;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.ConflictException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.IEntityNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.conflict.IFieldValueConflictFormatter;

public class FieldValueConflictException extends ConflictException {

    private IEntityNameFormatter entityNameFormatter;
    private IFieldNameFormatter fieldNameFormatter;
    private IFieldValueConflictFormatter messageFormatter;
    private Object providedValue;

    /**
     * A unique value was already present for another instance of the entity (409).
     * 
     * @param entityNameFormatter formatter for the entity name
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value which caused the conflict
     */
    public FieldValueConflictException(IEntityNameFormatter entityNameFormatter, IFieldNameFormatter fieldNameFormatter, IFieldValueConflictFormatter messageFormatter, Object providedValue){
        this.entityNameFormatter = entityNameFormatter;
        this.fieldNameFormatter = fieldNameFormatter;
        this.messageFormatter = messageFormatter;
        this.providedValue = providedValue;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFieldName(){
        return fieldNameFormatter.format();
    }

    /**
     * Gets the entity name.
     * 
     * @return the entity name.
     */
    public String getEntityName(){
        return entityNameFormatter.format();
    }

    /**
     * Gets the provided value.
     * 
     * @return the provided value which caused the conflict.
     */
    public Object getProvidedValue(){
        return providedValue;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format(getEntityName(), getFieldName(), providedValue);
    }

}
