package com.s4nderdevelopment.exceptionlib.exceptions.rest.server.internalservererror;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.server.InternalServerErrorException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.server.internalservererror.IUnknownErrorFormatter;

public class UnknownErrorException extends InternalServerErrorException {

    private IUnknownErrorFormatter messageFormatter;
    
    /**
     * An unknown error has occurred (500).
     * 
     * @param messageFormatter formatter for the status message
     */
    public UnknownErrorException(IUnknownErrorFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format();
    }

}
