package com.s4nderdevelopment.exceptionlib.exceptions.rest.client;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;

public class ConflictException extends RestException {

    private String message;

    /**
     * Conflict (409) exception for REST API's.
     */
    public ConflictException(){
        this.message = "Conflict";
    }

    /**
     * Conflict (409) exception for REST API's.
     * 
     * @param message a custom message.
     */
    public ConflictException(String message){
        this.message = message;
    }

    @Override
    public int getHttpStatusCode() {
        return 409;
    }

    @Override
    public String getFormattedMessage() {
        return message;
    }
}
