package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.forbidden;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.ForbiddenException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.forbidden.INoPermissionFormatter;

public class NoPermissionException extends ForbiddenException {
    
    private INoPermissionFormatter messageFormatter;

    /**
     * The requested entity was accessible (403).
     * 
     * @param messageFormatter formatter for the status message
     */
    public NoPermissionException(INoPermissionFormatter messageFormatter){
        this.messageFormatter = messageFormatter;
    }

    @Override
    public String getFormattedMessage(){
        return messageFormatter.format();
    }

}
