package com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.BadRequestException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMinFormatter;

public class IntRangeMinException extends BadRequestException {

    private IFieldNameFormatter fieldNameFormatter;
    private IIntRangeMinFormatter messageFormatter;
    private int providedValue;
    private int minValue;
    private Integer maxValue;

    /**
     * The provided value of an integer field was too low (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value, which was too low
     * @param minValue the minimum value
     */
    public IntRangeMinException(IFieldNameFormatter fieldNameFormatter, IIntRangeMinFormatter messageFormatter, int providedValue, int minValue){
        this(fieldNameFormatter, messageFormatter, providedValue, minValue, null);
    }

    /**
     * The provided value of an integer field was too low (400).
     * 
     * @param fieldNameFormatter formatter for the field name
     * @param messageFormatter formatter for the status message
     * @param providedValue the provided value, which was too low
     * @param minValue the minimum value
     * @param maxValue the maximum value, null if no maximum value
     */
    public IntRangeMinException(IFieldNameFormatter fieldNameFormatter, IIntRangeMinFormatter messageFormatter, int providedValue, int minValue, Integer maxValue){
        this.fieldNameFormatter = fieldNameFormatter;
        this.messageFormatter = messageFormatter;
        this.providedValue = providedValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /**
     * Gets the field name.
     * 
     * @return the field name.
     */
    public String getFieldName(){
        return fieldNameFormatter.format();
    }

    /**
     * Gets the provided value which was too low.
     * 
     * @return the provided value.
     */
    public int getProvidedValue(){
        return providedValue;
    }

    /**
     * Gets the minimum value.
     * 
     * @return the minimum value.
     */
    public int getMinValue(){
        return minValue;
    }

    /**
     * Gets the maximum value, or null.
     * 
     * @return the maximum value, or null.
     */
    public Integer getMaxValue(){
        return maxValue;
    }

    @Override
    public String getFormattedMessage() {
        return messageFormatter.format(getFieldName(), providedValue, minValue, maxValue);
    }
}
