package com.s4nderdevelopment.exceptionlib.formatters.rest.client.notfound;

public interface IEntityNotFoundFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param entityName name of the entity which was not found.
     * 
     * @return the formatted message.
     */
    public String format(String entityName);

}
