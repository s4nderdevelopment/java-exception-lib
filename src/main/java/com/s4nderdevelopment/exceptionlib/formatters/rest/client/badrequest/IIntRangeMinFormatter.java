package com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest;

public interface IIntRangeMinFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param fieldName name of the field with a value less than the minimum value.
     * @param providedValue the provided value which was less than the minimum value.
     * @param minValue the minimum allowed value.
     * @param maxValue the maximum allowed value, which is ignored when null.
     * 
     * @return the formatted message.
     */
    public String format(String fieldName, int providedValue, int minValue, Integer maxValue);

}
