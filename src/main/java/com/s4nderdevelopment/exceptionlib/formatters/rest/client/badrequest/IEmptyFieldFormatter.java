package com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest;

public interface IEmptyFieldFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param fieldName name of the field which was empty.
     * 
     * @return the formatted message.
     */
    public String format(String fieldName);

}
