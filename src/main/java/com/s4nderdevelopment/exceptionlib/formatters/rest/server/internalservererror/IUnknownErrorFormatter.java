package com.s4nderdevelopment.exceptionlib.formatters.rest.server.internalservererror;

public interface IUnknownErrorFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @return the formatted message.
     */
    public String format();

}
