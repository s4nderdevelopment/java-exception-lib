package com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized;

public interface INotLoggedInFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @return the formatted message.
     */
    public String format();

}
