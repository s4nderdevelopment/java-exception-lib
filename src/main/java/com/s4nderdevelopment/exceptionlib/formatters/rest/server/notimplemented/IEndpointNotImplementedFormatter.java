package com.s4nderdevelopment.exceptionlib.formatters.rest.server.notimplemented;

public interface IEndpointNotImplementedFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @return the formatted message.
     */
    public String format();

}
