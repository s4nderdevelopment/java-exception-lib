package com.s4nderdevelopment.exceptionlib.formatters.rest;

public interface IEntityNameFormatter {
    
    /**
     * Formats an entity and returns it as a String.
     * 
     * @return the formatted entity name.
     */
    public String format();

}
