package com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest;

public interface IStringInvalidValueFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param fieldName name of the field with an invalid value.
     * @param providedValue the invalid value of the field.
     * 
     * @return the formatted message.
     */
    public String format(String fieldName, String providedValue);

}
