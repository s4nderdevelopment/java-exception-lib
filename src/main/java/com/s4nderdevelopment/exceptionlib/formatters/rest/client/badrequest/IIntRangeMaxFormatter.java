package com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest;

public interface IIntRangeMaxFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param fieldName name of the field with a value greater than the maximum value.
     * @param providedValue the provided value which was greater than the maximum value.
     * @param maxValue the maximum allowed value.
     * @param minValue the minimum allowed value, which is ignored when null.
     * 
     * @return the formatted message.
     */
    public String format(String fieldName, int providedValue, int maxValue, Integer minValue);

}
