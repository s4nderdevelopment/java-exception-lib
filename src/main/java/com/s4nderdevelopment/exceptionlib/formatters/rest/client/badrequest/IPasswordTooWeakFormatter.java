package com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest;

public interface IPasswordTooWeakFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param fieldName name of the field with a too weak password value.
     * 
     * @return the formatted message.
     */
    public String format(String fieldName);

}
