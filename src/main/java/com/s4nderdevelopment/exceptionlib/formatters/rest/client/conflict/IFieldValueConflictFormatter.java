package com.s4nderdevelopment.exceptionlib.formatters.rest.client.conflict;

public interface IFieldValueConflictFormatter {
    
    /**
     * Formats a custom message and returns it as a String.
     * 
     * @param entityName name of the entity where the conflict occurred.
     * @param fieldName name of the field where the conflict occurred.
     * @param providedValue value of the field which caused the conflict.
     * 
     * @return the formatted message.
     */
    public String format(String entityName, String fieldName, Object providedValue);

}
