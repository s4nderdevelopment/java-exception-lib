package com.s4nderdevelopment.exceptionlib.formatters;

public interface IFieldNameFormatter {
    
    /**
     * Formats a field and returns it as a String.
     * 
     * @return the formatted field name.
     */
    public String format();

}
