package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidAuthenticationTokenFormatter;

public class InvalidAuthenticationTokenFormatter implements IInvalidAuthenticationTokenFormatter {

    @Override
    public String format() {
        return "Invalid authentication token provided.";
    }
    
}
