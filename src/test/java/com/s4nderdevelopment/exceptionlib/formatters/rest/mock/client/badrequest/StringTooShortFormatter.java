package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMinFormatter;

public class StringTooShortFormatter implements IIntRangeMinFormatter {

    public String format(String fieldName, int providedValue, int minValue, Integer maxValue) {
        if(maxValue == null){
            return "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " characters.";
        }
        return "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " and at most " + maxValue + " characters.";
    }
}
