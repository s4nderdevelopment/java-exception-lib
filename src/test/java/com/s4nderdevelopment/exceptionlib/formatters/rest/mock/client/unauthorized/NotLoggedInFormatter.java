package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.INotLoggedInFormatter;

public class NotLoggedInFormatter implements INotLoggedInFormatter {

    @Override
    public String format() {
        return "Log in to use this resource.";
    }
    
}
