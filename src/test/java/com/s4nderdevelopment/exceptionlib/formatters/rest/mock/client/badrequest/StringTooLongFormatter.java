package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMaxFormatter;

public class StringTooLongFormatter implements IIntRangeMaxFormatter {

    public String format(String fieldName, int providedValue, int maxValue, Integer minValue) {
        if(minValue == null){
            return "The field " + fieldName.toLowerCase() + " requires at most " + maxValue + " characters.";
        }
        return "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " and at most " + maxValue + " characters.";
    }
    
}
