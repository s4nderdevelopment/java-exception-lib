package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IStringInvalidValueFormatter;

public class InvalidEmailAddressFormatter implements IStringInvalidValueFormatter {

    public String format(String fieldName, String providedValue) {
        return "The value '" + providedValue + "' for field " + fieldName.toLowerCase() + " is not a valid email address.";
    }

}
