package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidCredentialsFormatter;

public class InvalidCredentialsFormatter implements IInvalidCredentialsFormatter {

    @Override
    public String format() {
        return "Invalid credentials provided.";
    }

}
