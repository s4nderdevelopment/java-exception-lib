package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.forbidden;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.forbidden.INoPermissionFormatter;

public class NoPermissionFormatter implements INoPermissionFormatter {

    @Override
    public String format() {
        return "No permission to use this resource.";
    }
    
}
