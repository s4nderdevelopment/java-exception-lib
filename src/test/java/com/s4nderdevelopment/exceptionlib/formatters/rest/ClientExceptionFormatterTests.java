package com.s4nderdevelopment.exceptionlib.formatters.rest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest.EmptyFieldException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest.IntRangeMaxException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest.IntRangeMinException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest.PasswordTooWeakException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.badrequest.StringInvalidValueException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.conflict.FieldValueConflictException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.forbidden.NoPermissionException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.notfound.EntityNotFoundException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized.InvalidAuthenticationTokenException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized.InvalidCredentialsException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.client.unauthorized.NotLoggedInException;
import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IEmptyFieldFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMaxFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IIntRangeMinFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IPasswordTooWeakFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IStringInvalidValueFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.conflict.IFieldValueConflictFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.forbidden.INoPermissionFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.notfound.IEntityNotFoundFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidAuthenticationTokenFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.IInvalidCredentialsFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.client.unauthorized.INotLoggedInFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.EntityNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.FieldNameFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.EmptyFieldFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.InvalidEmailAddressFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.InvalidOptionFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.PasswordTooWeakFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.StringTooLongFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest.StringTooShortFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.conflict.AlreadyExistsFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.forbidden.NoPermissionFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.notfound.EntityNotFoundFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized.InvalidAuthenticationTokenFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized.InvalidCredentialsFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.unauthorized.NotLoggedInFormatter;

import org.junit.jupiter.api.Test;

public class ClientExceptionFormatterTests {
    
    @Test
    @DisplayName("Test exception format when first name is empty")
    void testFormatFirstNameEmpty(){
        // Arrange
        String fieldName = "First name";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IEmptyFieldFormatter messageFormatter = new EmptyFieldFormatter();

        String expectedMessage = "The field " + fieldName.toLowerCase() + " was empty.";

        // Act
        try{
            throw new EmptyFieldException(fieldNameFormatter, messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
    
    @Test
    @DisplayName("Test exception format when first name is too short (1)")
    void testFormatFirstNameTooShortWithMaxValue(){
        // Arrange
        String fieldName = "First name";
        int minValue = 2;
        Integer maxValue = 32;
        String providedValue = "B";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IIntRangeMinFormatter messageFormatter = new StringTooShortFormatter();

        String expectedMessage = "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " and at most " + maxValue + " characters.";

        // Act
        try{
            throw new IntRangeMinException(fieldNameFormatter, messageFormatter, providedValue.length(), minValue, maxValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
    
    @Test
    @DisplayName("Test exception format when first name is too short (2)")
    void testFormatFirstNameTooShortWithoutMaxValue(){
        // Arrange
        String fieldName = "First name";
        int minValue = 2;
        String providedValue = "B";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IIntRangeMinFormatter messageFormatter = new StringTooShortFormatter();

        String expectedMessage = "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " characters.";

        // Act
        try{
            throw new IntRangeMinException(fieldNameFormatter, messageFormatter, providedValue.length(), minValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
    
    @Test
    @DisplayName("Test exception format when first name is too long (1)")
    void testFormatFirstNameTooLongWithMinValue(){
        // Arrange
        String fieldName = "First name";
        int maxValue = 32;
        Integer minValue = 2;
        String providedValue = "Bob with some extra characters behind his name to exceed the 32 character limit";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IIntRangeMaxFormatter messageFormatter = new StringTooLongFormatter();

        String expectedMessage = "The field " + fieldName.toLowerCase() + " requires at least " + minValue + " and at most " + maxValue + " characters.";

        // Act
        try{
            throw new IntRangeMaxException(fieldNameFormatter, messageFormatter, providedValue.length(), maxValue, minValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
    
    @Test
    @DisplayName("Test exception format when first name is too long (2)")
    void testFormatFirstNameTooLongWithoutMinValue(){
        // Arrange
        String fieldName = "First name";
        int maxValue = 32;
        String providedValue = "Bob with some extra characters behind his name to exceed the 32 character limit";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IIntRangeMaxFormatter messageFormatter = new StringTooLongFormatter();

        String expectedMessage = "The field " + fieldName.toLowerCase() + " requires at most " + maxValue + " characters.";

        // Act
        try{
            throw new IntRangeMaxException(fieldNameFormatter, messageFormatter, providedValue.length(), maxValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when project visibility is set to an invalid option")
    void testFormatProjectVisibilityInvalidOption(){
        // Arrange
        String fieldName = "Project visibility";
        String[] allowedOptions = new String[]{ "private", "public" };
        String providedValue = "internal";
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IStringInvalidValueFormatter messageFormatter = new InvalidOptionFormatter(allowedOptions);

        String expectedMessage = "The value '" + providedValue + "' for field " + fieldName.toLowerCase() + " is invalid. Allowed values are: " + allowedOptions[0] + ", " + allowedOptions[1];

        // Act
        try{
            throw new StringInvalidValueException(fieldNameFormatter, messageFormatter, providedValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the provided email address is invalid")
    void testFormatEmailAddressInvalid(){
        // Arrange
        String fieldName = "Email address";
        String providedValue = "user@example."; // Invalid email, ends with a dot.
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        IStringInvalidValueFormatter messageFormatter = new InvalidEmailAddressFormatter();

        String expectedMessage = "The value '" + providedValue + "' for field " + fieldName.toLowerCase() + " is not a valid email address.";

        // Act
        try{
            throw new StringInvalidValueException(fieldNameFormatter, messageFormatter, providedValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the provided email address is already used")
    void testFormatEmailAddressConflict(){
        // Arrange
        String entityName = "User";
        String fieldName = "Email address";
        String providedValue = "user@example.com"; // User with this email address already exists
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);
        IEntityNameFormatter entityNameFormatter = new EntityNameFormatter(entityName);

        IFieldValueConflictFormatter messageFormatter = new AlreadyExistsFormatter();

        String expectedMessage = "A " + entityName.toLowerCase() + " with " + fieldName.toLowerCase() + " '" + providedValue.toString() + "' already exists.";

        // Act
        try{
            throw new FieldValueConflictException(entityNameFormatter, fieldNameFormatter, messageFormatter, providedValue);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the authorization header value has an invalid or expired token")
    void testFormatAuthorizationFailed(){
        // Arrange
        IInvalidAuthenticationTokenFormatter messageFormatter = new InvalidAuthenticationTokenFormatter();

        String expectedMessage = "Invalid authentication token provided.";

        // Act
        try{
            throw new InvalidAuthenticationTokenException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the user is not logged in")
    void testFormatNotLoggedIn(){
        // Arrange
        INotLoggedInFormatter messageFormatter = new NotLoggedInFormatter();

        String expectedMessage = "Log in to use this resource.";

        // Act
        try{
            throw new NotLoggedInException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the user has no permission to use the resource")
    void testFormatNoPermission(){
        // Arrange
        INoPermissionFormatter messageFormatter = new NoPermissionFormatter();

        String expectedMessage = "No permission to use this resource.";

        // Act
        try{
            throw new NoPermissionException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when a requested entity was not found")
    void testFormatEntityNotFound(){
        // Arrange
        IEntityNotFoundFormatter messageFormatter = new EntityNotFoundFormatter();

        IEntityNameFormatter entityNameFormatter = new EntityNameFormatter("user");

        String expectedMessage = "User not found.";

        // Act
        try{
            throw new EntityNotFoundException(messageFormatter, entityNameFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the user has provided invalid credentials")
    void testFormatInvalidCredentials(){
        // Arrange
        IInvalidCredentialsFormatter messageFormatter = new InvalidCredentialsFormatter();

        String expectedMessage = "Invalid credentials provided.";

        // Act
        try{
            throw new InvalidCredentialsException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }

    @Test
    @DisplayName("Test exception format when the user has provided a weak password")
    void testFormatPasswordTooWeak(){
        // Arrange
        String fieldName = "Password";

        IPasswordTooWeakFormatter messageFormatter = new PasswordTooWeakFormatter();
        IFieldNameFormatter fieldNameFormatter = new FieldNameFormatter(fieldName);

        String expectedMessage = "That " + fieldName.toLowerCase() + " is too weak. It should at least contain letters and numbers.";

        // Act
        try{
            throw new PasswordTooWeakException(fieldNameFormatter, messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
}
