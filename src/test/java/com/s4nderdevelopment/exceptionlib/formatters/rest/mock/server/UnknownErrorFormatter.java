package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.server;

import com.s4nderdevelopment.exceptionlib.formatters.rest.server.internalservererror.IUnknownErrorFormatter;

public class UnknownErrorFormatter implements IUnknownErrorFormatter {

    @Override
    public String format() {
        return "An unknown error occurred.";
    }
    
}
