package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.conflict;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.conflict.IFieldValueConflictFormatter;

public class AlreadyExistsFormatter implements IFieldValueConflictFormatter {

    public String format(String entityName, String fieldName, Object providedValue) {
        String entityNameLower = entityName.toLowerCase();
        
        if(entityNameLower.startsWith("a") || entityNameLower.startsWith("e") || entityNameLower.startsWith("i") || entityNameLower.startsWith("o")){
            return "An " + entityNameLower + " with " + fieldName.toLowerCase() + " '" + providedValue.toString() + "' already exists.";
        }

        return "A " + entityNameLower + " with " + fieldName.toLowerCase() + " '" + providedValue.toString() + "' already exists.";
    }
}
