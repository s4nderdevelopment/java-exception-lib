package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IStringInvalidValueFormatter;

public class InvalidOptionFormatter implements IStringInvalidValueFormatter {

    private String[] allowedOptions;

    public InvalidOptionFormatter(String... allowedOptions){
        this.allowedOptions = allowedOptions;
    }

    private String getFormattedAllowedOptions(){
        String result = "";

        if(allowedOptions == null || allowedOptions.length == 0){
            return "";
        }

        for(int i = 0; i < allowedOptions.length - 1; i++){
            result += allowedOptions[i] + ", ";
        }
        result += allowedOptions[allowedOptions.length - 1];

        return result;
    }

    public String format(String fieldName, String providedValue) {
        return "The value '" + providedValue + "' for field " + fieldName.toLowerCase() + " is invalid. Allowed values are: " + getFormattedAllowedOptions();
    }

}
