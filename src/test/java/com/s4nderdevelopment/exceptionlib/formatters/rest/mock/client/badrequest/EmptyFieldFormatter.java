package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IEmptyFieldFormatter;

public class EmptyFieldFormatter implements IEmptyFieldFormatter {

    public String format(String fieldName) {
        return "The field " + fieldName.toLowerCase() + " was empty.";
    }
}
