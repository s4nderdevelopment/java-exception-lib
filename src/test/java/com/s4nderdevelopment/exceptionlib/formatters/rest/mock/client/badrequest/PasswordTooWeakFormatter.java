package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.badrequest;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.badrequest.IPasswordTooWeakFormatter;

public class PasswordTooWeakFormatter implements IPasswordTooWeakFormatter {

    @Override
    public String format(String fieldName) {
        return "That " + fieldName.toLowerCase() + " is too weak. It should at least contain letters and numbers.";
    }
    
}
