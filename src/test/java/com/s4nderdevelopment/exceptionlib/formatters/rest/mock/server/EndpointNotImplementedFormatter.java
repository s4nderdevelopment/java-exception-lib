package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.server;

import com.s4nderdevelopment.exceptionlib.formatters.rest.server.notimplemented.IEndpointNotImplementedFormatter;

public class EndpointNotImplementedFormatter implements IEndpointNotImplementedFormatter {

    @Override
    public String format() {
        return "This endpoint cannot be used, because no functionality has been implemented yet.";
    }
    
}
