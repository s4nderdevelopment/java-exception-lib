package com.s4nderdevelopment.exceptionlib.formatters.rest.mock;

import com.s4nderdevelopment.exceptionlib.formatters.rest.IEntityNameFormatter;

public class EntityNameFormatter implements IEntityNameFormatter {

    private String fieldName;

    public EntityNameFormatter(String fieldName){
        this.fieldName = fieldName;
    }

    public String format() {
        return fieldName;
    }
}
