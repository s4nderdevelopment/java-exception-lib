package com.s4nderdevelopment.exceptionlib.formatters.rest.mock;

import com.s4nderdevelopment.exceptionlib.formatters.IFieldNameFormatter;

public class FieldNameFormatter implements IFieldNameFormatter {

    private String fieldName;

    public FieldNameFormatter(String fieldName){
        this.fieldName = fieldName;
    }

    public String format() {
        return fieldName;
    }
}
