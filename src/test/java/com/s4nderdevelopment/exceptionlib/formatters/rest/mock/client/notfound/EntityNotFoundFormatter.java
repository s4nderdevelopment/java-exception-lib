package com.s4nderdevelopment.exceptionlib.formatters.rest.mock.client.notfound;

import com.s4nderdevelopment.exceptionlib.formatters.rest.client.notfound.IEntityNotFoundFormatter;

public class EntityNotFoundFormatter implements IEntityNotFoundFormatter {

    @Override
    public String format(String entityName) {
        String entityCapitalized = entityName.length() == 0 ? "" : (entityName.charAt(0) + "").toUpperCase() + entityName.substring(1);
        return entityCapitalized + " not found.";
    }
    
}
