package com.s4nderdevelopment.exceptionlib.formatters.rest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import com.s4nderdevelopment.exceptionlib.exceptions.rest.RestException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.server.internalservererror.UnknownErrorException;
import com.s4nderdevelopment.exceptionlib.exceptions.rest.server.notimplemented.EndpointNotImplementedException;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.server.EndpointNotImplementedFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.mock.server.UnknownErrorFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.server.internalservererror.IUnknownErrorFormatter;
import com.s4nderdevelopment.exceptionlib.formatters.rest.server.notimplemented.IEndpointNotImplementedFormatter;

import org.junit.jupiter.api.Test;

public class ServerExceptionFormatterTests {
    
    @Test
    @DisplayName("Test exception format when an unknown error occurs")
    void testFormatUnknownError(){
        // Arrange
        IUnknownErrorFormatter messageFormatter = new UnknownErrorFormatter();

        String expectedMessage = "An unknown error occurred.";

        // Act
        try{
            throw new UnknownErrorException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
    
    @Test
    @DisplayName("Test exception format when an not yet implemented API endpoint is called")
    void testFormatNotImplementedEndpointCalled(){
        // Arrange
        IEndpointNotImplementedFormatter messageFormatter = new EndpointNotImplementedFormatter();

        String expectedMessage = "This endpoint cannot be used, because no functionality has been implemented yet.";

        // Act
        try{
            throw new EndpointNotImplementedException(messageFormatter);
        }catch(RestException e){
            String actualMessage = e.getFormattedMessage();

            // Assert
            Assertions.assertEquals(expectedMessage, actualMessage, "Expected: " + expectedMessage + "\nGot: " + actualMessage);
        }
    }
}
