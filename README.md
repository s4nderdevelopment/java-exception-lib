# ExceptionLib

ExceptionLib is a library which provides useful exception classes for your java projects. It also provides an easy way to format your exception messages in multiple languages for multi-language applications.

## Using this library

Add the snippet below to your pom.xml:

```xml
<!-- https://mvnrepository.com/artifact/com.s4nderdevelopment/exception-lib -->
<dependency>
  <groupId>com.s4nderdevelopment</groupId>
  <artifactId>exception-lib</artifactId>
  <version>VERSION</version>
</dependency>
```

Replace `REPLACE-VERSION` with the version you would like to use. For the latest version, see https://mvnrepository.com/artifact/com.s4nderdevelopment/exception-lib

## Generating documentation

Run `mvn clean javadoc:javadoc`. Now the javadoc documentation can be found under `target/site/apidocs`. To view, open `index.html` in your browser.

## (Current) list of exceptions

- REST Exceptions (for REST API's)
  - Status code 4XX
    - 400: BadRequestException
      - EmptyFieldException: A field value was not provided in the request body, or the value was empty.
      - IntRangeMaxException: An integer value was too big.
      - IntRangeMinException: An integer value was too small.
      - PasswordTooWeakException: The provided password was too weak.
      - StringInvalidValueException: A string value was not allowed. For example: an email address was required, but the provided value did not contain an '@' character.
    - 401: UnauthorizedException
      - InvalidAuthenticationTokenException: The authentication token of the current user is not valid.
      - InvalidCredentialsException: The user tried to log in, but provided invalid credentials.
      - NotLoggedInException: The current user is not allowed to use the resource, because they are not logged in.
    - 403: ForbiddenException
      - NoPermissionException: The current user is not allowed to use the resource.
    - 404: NotFoundException
      - EntityNotFoundException: The requested entity was not found.
    - 405: MethodNotAllowedException
    - 409: ConflictException
      - FieldValueConflictException: A unique field was already used, for example an email address for a user account.
  - Status code 5XX
    - 500: InternalServerErrorException
      - UnknownErrorException: An unknown error occurred on the server.
    - 501: NotImplementedException
      - EndpointNotImplementedException: A REST API endpoint was not implemented yet.

## TODO's

- [x] Add javadoc pages
- [ ] Add more general exceptions for non-web applications
